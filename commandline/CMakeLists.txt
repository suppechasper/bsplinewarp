
#2D executables
ADD_EXECUTABLE(BSplineImageTransform_2D ImageTransform.cxx)
TARGET_LINK_LIBRARIES (BSplineImageTransform_2D ${ITK_LIBRARIES}  )

ADD_EXECUTABLE(BSplineRegister_2D Register.cxx)
TARGET_LINK_LIBRARIES (BSplineRegister_2D ${ITK_LIBRARIES}  )

#ADD_EXECUTABLE(BSplineLSFit_2D BSplineLSFit.cxx)
#TARGET_LINK_LIBRARIES (BSplineLSFit_2D ${ITK_LIBRARIES}  gfortran lapack blas)

SET_TARGET_PROPERTIES(BSplineImageTransform_2D BSplineRegister_2D 
                      PROPERTIES COMPILE_FLAGS -DDIMENSION=2)



#3D executables
  
ADD_EXECUTABLE(BSplineImageTransform_3D ImageTransform.cxx)
TARGET_LINK_LIBRARIES (BSplineImageTransform_3D ${ITK_LIBRARIES}  )

ADD_EXECUTABLE(BSplineRegister_3D Register.cxx)
TARGET_LINK_LIBRARIES (BSplineRegister_3D ${ITK_LIBRARIES}  )

#ADD_EXECUTABLE(BSplineLSFit_3D BSplineLSFit.cxx)
#TARGET_LINK_LIBRARIES (BSplineLSFit_3D ${ITK_LIBRARIES}  gfortran lapack blas)

SET_TARGET_PROPERTIES(BSplineImageTransform_3D BSplineRegister_3D 
                      PROPERTIES COMPILE_FLAGS -DDIMENSION=3)
