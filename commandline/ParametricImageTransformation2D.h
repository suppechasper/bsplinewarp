#ifndef PARAMETRICIMAGETRANSFORMATION2D_H
#define PARAMETRICIMAGETRANSFORMATION2D_H

#include "itkImage.h"
#include "itkCastImageFilter.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkLinearInterpolateImageFunction.h"



template <typename TParametric, typename TImage>
class ParametricImageTransformation2D{
  public:
    typedef typename TParametric::TKnotVector TKnotVector;
    typedef typename TParametric::TControlMesh TControlMesh;
    typedef typename TParametric::TControlPoint TControlPoint;
    typedef typename TControlPoint::TPrecision TPrecision;  
    
    typedef TImage Image;
    typedef typename Image::Pointer ImagePointer;
    typedef typename Image::IndexType ImageIndex; 
    typedef typename Image::PointType ImagePoint;
    typedef typename Image::RegionType ImageRegion;
    typedef typename ImageRegion::SizeType ImageSize;
    typedef typename ImageSize::SizeValueType ImageSizeValue;

    typedef itk::Image<TPrecision *, 2> BFImage;
    typedef typename BFImage::Pointer BFImagePointer;
    
    typedef itk::Image<int, 2> SpanImage;
    typedef typename SpanImage::Pointer SpanImagePointer; 


    ParametricImageTransformation2D(){ 
      useMask = false;
      image = NULL;
    };

    void SetImage(ImagePointer image){  
	    this->image = image; 
    };
 
    void SetRange(ImageRegion range){
	    this->range = range;
    };


    ImageRegion GetRange(){
	    return range;
    };


    ImagePoint GetSize(){
	    return size;
    };


    ImagePoint GetStart(){
        return start;
    };

    
    void SetMaskImage(ImagePointer maskImage) { 
      this->maskImage = maskImage; 
      CastFilterPointer castFilter = CastFilter::New();
      castFilter->SetInput(maskImage);
      castFilter->Update();
      transformedMaskImage = castFilter->GetOutput();
    };


    
    ImagePointer GetTransformedMaskImage(){ 
      return transformedMaskImage; 
    };


    void SetUseMask(bool use){
      //useMask = use;
    };


    void SetParametric(TParametric &surface){ 
      this->surface = surface; 
    };

    
    TParametric &GetParametric(){ return surface; };
   
    ImagePointer Transform();
    

    void Transform(ImagePointer transformed);
    
    void TransformAndStoreBFs(ImagePointer transformed, BFImagePointer uImage,
        BFImagePointer vImage, SpanImagePointer uspans, SpanImagePointer vspans);

    ImageRegion GetImageRegion(TPrecision uStart, TPrecision uEnd, TPrecision vStart, TPrecision vEnd);


    //Operators
    ParametricImageTransformation2D<TParametric, TImage>& operator=(const ParametricImageTransformation2D<TParametric, TImage>& rhs){
       surface = rhs.surface;
       image = rhs.image;
       maskImage = rhs.maskImage;
       transformedMaskImage = rhs.transformedMaskImage;
       range = rhs.range;
       start = rhs.start;
       size = rhs.size;
       useMask = rhs.useMask;
    };



    friend std::ostream& operator << (std::ostream&
      os, ParametricImageTransformation2D<TParametric, TImage>& transform){

       os << transform.size[0] << std::endl;
       os << transform.size[1] << std::endl;
       os << transform.start[0] << std::endl;
       os << transform.start[1] << std::endl;
       os << transform.surface << std::endl;
       

       std::cout << "transform written" << std::endl;

       return os;
    };



    friend std::istream& operator >> (std::istream& is, 
        ParametricImageTransformation2D<TParametric, TImage>& transform){
        
        ImageSize size = transform.range.GetSize();
        ImageIndex rangeIndex = transform.range.GetIndex();

        is >> transform.size[0];
        is >> transform.size[1];
        is >> transform.start[0];
        is >> transform.start[1];

        is >> transform.surface;
        transform.useMask = false;
        return is;

    };




    void ComputePhysicalRange(ImageRegion range){
	ImageIndex index = range.GetIndex();
	image->TransformIndexToPhysicalPoint(index, start);
	for(unsigned int i=0; i< Image::GetImageDimension(); i++){
	  index[i] += range.GetSize(i);
	} 
	image->TransformIndexToPhysicalPoint(index, size);
	size[0] -= start[0];
        size[1] -= start[1];
    };



  private:
    TParametric surface;
    ImagePointer image;
    ImagePointer maskImage;
    ImagePointer transformedMaskImage;
    ImagePoint start;
    ImagePoint size;
    ImageRegion range;

    //CastFilter for copying Images
    typedef itk::CastImageFilter<Image, Image> CastFilter;
    typedef typename CastFilter::Pointer CastFilterPointer;

    typedef itk::ImageRegionIteratorWithIndex<Image> ImageIterator;
    typedef itk::ImageRegionIterator<BFImage> BFImageIterator;
    typedef itk::ImageRegionIterator<SpanImage> SpanImageIterator;


    typedef itk::LinearInterpolateImageFunction<Image, TPrecision> InterpolateFunction;
    typedef typename InterpolateFunction::Pointer InterpolateFunctionPointer;
    typedef typename InterpolateFunction::ContinuousIndexType ImageContinuousIndex;

    bool useMask;





};



//Non-inline implementations
template <typename TParametric, typename TImage>
typename ParametricImageTransformation2D<TParametric, TImage>::ImagePointer
ParametricImageTransformation2D<TParametric, TImage>::Transform(){
  if( image.IsNull() ){
    return NULL;
  }

  CastFilterPointer castFilter = CastFilter::New();
  castFilter->SetInput( image );
  castFilter->Update();
  ImagePointer transformed = castFilter->GetOutput();

  this->Transform(transformed);
  return transformed;

}


template <typename TParametric, typename TImage>
typename ParametricImageTransformation2D<TParametric, TImage>::ImageRegion
ParametricImageTransformation2D<TParametric, TImage>::GetImageRegion(TPrecision uStart, TPrecision uEnd, TPrecision vStart, TPrecision vEnd) 
{
  
  TKnotVector &knotsU =  surface.GetUKnots();
  TKnotVector &knotsV =  surface.GetVKnots();

  //Max/min knot values
  TPrecision uMin = knotsU.GetKnotA();
  TPrecision uMax = knotsU.GetKnotB();
  TPrecision uStep = ( uMax - uMin ) / size[0]; 

  TPrecision vMin = knotsV.GetKnotA();
  TPrecision vMax = knotsV.GetKnotB();
  TPrecision vStep = ( vMax - vMin ) / size[1]; 

  //Compute imageregion from physical coordinates
  ImagePoint tmp;
  tmp[0] = (long) ceil( start[0] + (uStart - uMin) / uStep );
  tmp[1] = (long) ceil( start[1] + (vStart - vMin) / vStep );

  ImageIndex regionIndex;
  image->TransformPhysicalPointToIndex(tmp, regionIndex);
  
  tmp[0] = (long) ceil( start[0] + (uEnd - uMin) / uStep );
  tmp[1] = (long) ceil( start[1] + (vEnd - vMin) / vStep );

  ImageIndex endIndex;
  image->TransformPhysicalPointToIndex(tmp, endIndex);


  ImageRegion region;
  region.SetIndex(regionIndex);
  region.SetSize(0, endIndex[0] - regionIndex[0] );
  region.SetSize(1, endIndex[1] - regionIndex[1] );

  return region; 
}



template <typename TParametric, typename TImage>
void
ParametricImageTransformation2D<TParametric, TImage>::Transform(ImagePointer
    transformed){
 InterpolateFunctionPointer imageip = InterpolateFunction::New();  
 InterpolateFunctionPointer maskip = InterpolateFunction::New();
  
 
  std::cout<<"image region: " << image->GetLargestPossibleRegion() << std::endl; 
  imageip->SetInputImage( image );
  if(useMask){
    maskip->SetInputImage( maskImage );
  }

  
  TKnotVector &knotsU =  surface.GetUKnots();
  TKnotVector &knotsV =  surface.GetVKnots();

  TPrecision uMin = knotsU.GetKnotA();
  TPrecision uMax = knotsU.GetKnotB();
  TPrecision uStep = ( uMax - uMin ) / size[0]; 

  TPrecision vMin = knotsV.GetKnotA();
  TPrecision vMax = knotsV.GetKnotB();
  TPrecision vStep = ( vMax - vMin ) / size[1]; 
  
  ImageRegion region = transformed->GetLargestPossibleRegion();
  ImageSize size = region.GetSize();
  ImageIndex index = region.GetIndex();

  TControlPoint out; 
  ImageIterator it( transformed, range );

  ImageIterator maskIt;
  if(useMask){
    maskIt = ImageIterator( transformedMaskImage, range );
  }
  
  int p = surface.GetDegreeU();
  int q = surface.GetDegreeV();

  TPrecision *bfu = new TPrecision[p+1]; 
  TPrecision *bfv = new TPrecision[q+1];
  int dummy1 = 0; 
  int dummy2 = 0;
  ImagePoint pnt;



  for(it.GoToBegin(); !it.IsAtEnd(); ++it){
    ImageIndex current = it.GetIndex();
    transformed->TransformIndexToPhysicalPoint(current, pnt);
    //std::cout << "index: " << current << std::endl;
    //std::cout << "rangeindex: " << rangeIndex << std::endl;
    TPrecision u = uMin + ( pnt[0] - start[0] ) * uStep;
    TPrecision v = vMin + ( pnt[1] - start[1] ) * vStep;

    surface.PointAt( u, v, out, dummy1, dummy2, bfu, bfv);
    
    pnt[0] = out.x;
    pnt[1] = out.y;

    TPrecision pixel = (TPrecision) imageip->Evaluate( pnt );
    it.Set( pixel );
   
    if(useMask){ 
      pixel = (TPrecision) maskip->Evaluate( pnt ); 
      maskIt.Set( pixel );
      ++maskIt;
    }

  }

}


template <typename TParametric, typename TImage>
void 
ParametricImageTransformation2D<TParametric, TImage>::TransformAndStoreBFs(
    ImagePointer transformed, BFImagePointer uImage, BFImagePointer vImage,
    SpanImagePointer suImage, SpanImagePointer svImage )
{

  InterpolateFunctionPointer imageip = InterpolateFunction::New();  
 InterpolateFunctionPointer maskip = InterpolateFunction::New();

  imageip->SetInputImage(image );
  if(useMask){
    maskip->SetInputImage (maskImage );
  }

  
  TKnotVector &knotsU =  surface.GetUKnots();
  TKnotVector &knotsV =  surface.GetVKnots();

  TPrecision uMin = knotsU.GetKnotA();
  TPrecision uMax = knotsU.GetKnotB();
  TPrecision uStep = ( uMax - uMin ) / size[0]; 

  TPrecision vMin = knotsV.GetKnotA();
  TPrecision vMax = knotsV.GetKnotB();
  TPrecision vStep = ( vMax - vMin ) / size[1]; 
  
  
  BFImageIterator uit(uImage, range);
  BFImageIterator vit(vImage, range);
  SpanImageIterator suit(suImage, range);
  SpanImageIterator svit(svImage, range);
  
  int vspan = 0;
  int uspan = 0;
  TControlPoint pOut;
  ImageIterator it( transformed, range );

  ImageIterator maskIt;
  if(useMask){
    maskIt = ImageIterator( transformedMaskImage, range );
  }
  ImageRegion region = transformed->GetLargestPossibleRegion();
  ImageSize size = region.GetSize();

  

  int p = surface.GetDegreeU();
  int q = surface.GetDegreeV();
  ImagePoint pnt;
  for(it.GoToBegin(); !it.IsAtEnd(); ++it, ++uit, ++vit, ++suit, ++svit ){

    ImageIndex current = it.GetIndex();
    transformed->TransformIndexToPhysicalPoint(current, pnt);
    TPrecision u = uMin + ( pnt[0] - start[0] ) * uStep;
    TPrecision v = vMin + ( pnt[1] - start[1] ) * vStep;
    TPrecision *bfu = new TPrecision[p+1]; 
    TPrecision *bfv = new TPrecision[q+1]; 
   
    surface.PointAt( u, v, pOut, uspan, vspan, bfu, bfv );

    pnt[0] = pOut.x;
    pnt[1] = pOut.y;

    TPrecision pixel = (TPrecision) imageip->Evaluate( pnt );
    it.Set( pixel );


   if(useMask){ 
      pixel = (TPrecision) maskip->Evaluate( pnt );
      maskIt.Set( pixel );
      ++maskIt;
   }


    delete[] uit.Get();
    delete[] vit.Get();
    uit.Set(bfu);
    vit.Set(bfv);


    suit.Set(uspan);
    svit.Set(vspan);


  }
  
}

#endif

