#define USE_ORIENTED

#include "MyConfig.h"

#include "ImageIO.h"

#include <fstream>

#include "Point2D.h"
#include "Point3D.h"

#include "BSpline3D.h"
#include "BSpline2D.h"
#include "ParametricImageTransformation3D.h"
#include "ParametricImageTransformation2D.h"

//Command line parsing
#include <tclap/CmdLine.h>


#if DIMENSION == 3
  typedef Point3D< Precision > TControlPoint;
  typedef BSpline3D<TControlPoint, Precision> TParametric;
  typedef ParametricImageTransformation3D<TParametric, Image> TTransformation;
#else
  typedef Point2D< Precision > TControlPoint;
  typedef BSpline2D<TControlPoint> TParametric;
  typedef ParametricImageTransformation2D<TParametric, Image> TTransformation;
#endif


typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef Image::SizeType ImageSize;
typedef Image::IndexType ImageIndex;

int main(int argc, char **argv){
 try{ 
  if(argc != 5){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " inputimage outputfilename transformfile isTransform";
    std::cout << "isTransform" << std::endl;
    return 0;
  }

  bool isTransform = atoi(argv[4]) !=0 ;

  //Read Input Image
  ImagePointer input = ImageIO<Image>::readImage( argv[1] );
  //ImagePointer output = ImageIO<Image>::readImage( argv[1] );
  ImageRegion region = input->GetLargestPossibleRegion();
  ImageSize regionSize = region.GetSize();
  ImageIndex regionIndex = region.GetIndex();



  //read surface
  TTransformation transform;
  std::ifstream file;
  file.open(argv[3]);


  if(isTransform){
    file >> transform;
    transform.SetRange(region);
  }
  else{
    TParametric surface;
    file >> surface;
    transform.SetParametric(surface);
    transform.SetRange(region);
  }

  file.close();
  

  transform.SetImage( input );
  
  //Setup Image transformation
  std::cout << "Starting transformation" << std::endl;
  ImagePointer output = transform.Transform();
  std::cout << "Finished transformation" << std::endl;
  

  //Write ouput image
  ImageIO<Image>::saveImage(output, argv[2]);

  return 0;
 }
 catch(const char *err){
  std::cout << err << std::endl;
 }
}

