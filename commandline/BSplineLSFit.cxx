#include "MyConfig.h"

#include "ControlPoint1D.h"
#include "Point3D.h"


#include "BSpline3D.h"
#include "BSpline2D.h"
#include "ImageIO.h"
#include "Linalg.h"

#include <tclap/CmdLine.h>

#include <string>

#include "itkImageRegionConstIteratorWithIndex.h"



typedef ControlPoint1D<Precision> TControlPoint;
#if DIMENSION == 3
  typedef BSpline3D<TControlPoint> BSpline;
#else
  typedef BSpline2D<TControlPoint> BSpline;
#endif

typedef BSpline::TControlMesh ControlMesh;
typedef BSpline::TKnotVector Knots;

typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef Image::SizeType ImageSize;
typedef Image::IndexType ImageIndex;

int main(int argc, char **argv){
  using namespace FortranLinalg;

  TCLAP::CmdLine cmd("Least Squares BSpline fit", ' ', "1");

  TCLAP::ValueArg<int> nxArg("", "nx", "number of controlpoints in x direction",
      true, 0, "integer");
  cmd.add(nxArg);

  TCLAP::ValueArg<int> nyArg("", "ny", "number of controlpoints in y direction",
      true, 0, "integer");
  cmd.add(nyArg);
    

#if DIMENSION == 3
  TCLAP::ValueArg<int> nzArg("", "nz", "number of controlpoints in z direction",
      true, 0, "integer");
  cmd.add(nzArg);
#endif


  TCLAP::ValueArg<int> ixArg("", "ix", "start index in x direction",
      false, -1, "integer");
  cmd.add(ixArg);

  TCLAP::ValueArg<int> iyArg("", "iy", "start index in y direction",
      false, -1, "integer");
  cmd.add(iyArg);
    

#if DIMENSION == 3
  TCLAP::ValueArg<int> izArg("", "iz", "start index in z direction",
      false, -1, "integer");
  cmd.add(izArg);
#endif


  TCLAP::ValueArg<int> sxArg("", "sx", "size index in x direction",
      false, -1, "integer");
  cmd.add(sxArg);

  TCLAP::ValueArg<int> syArg("", "sy", "size index in y direction",
      false, -1, "integer");
  cmd.add(syArg);
    

#if DIMENSION == 3
  TCLAP::ValueArg<int> szArg("", "sz", "size index in z direction",
      false, -1, "integer");
  cmd.add(szArg);
#endif


  TCLAP::ValueArg<int> degreeArg("d", "degree", "degree of bspline", false,2, "integer");
  cmd.add(degreeArg);

  TCLAP::ValueArg<std::string> inputArg("i", "input", "input image to fit", true, "", "filename");  
  cmd.add(inputArg);
  
  TCLAP::ValueArg<std::string> outputArg("o", "output", "output image", true, "", "filename");  
  cmd.add(outputArg);

  TCLAP::ValueArg<unsigned int> subsampleArg("s", "subsample", "subsample image", false, 1, "factor");  
  cmd.add(subsampleArg);


  try{
    cmd.parse(argc, argv);
  }
  catch(TCLAP::ArgException &e){
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }



  /*{
  //least squares test
    DenseMatrix<Precision> A(3, 2);
    DenseMatrix<Precision> b(3, 1);
    A(0, 0) = 1;
    A(0, 1) = 0;
    A(1, 0) = 1;
    A(1, 1) = 1;
    A(2, 0) = 1;
    A(2, 1) = 2;
    b(0, 0) = 1;
    b(1, 0) = 2;
    b(2, 0) = 1.5;
    DenseMatrix<Precision> x = Linalg<Precision>::LeastSquares(A, b);
    for(int i=0; i<x.M(); i++){
     for(int j=0; j<x.N(); j++){
       std::cout << x(i, j) << std::endl;
     }
    }
  }*/

  int ncps[DIMENSION];
  ncps[0] = nxArg.getValue();
  ncps[1] = nyArg.getValue();
#if DIMENSION == 3
  ncps[2] = nzArg.getValue();
#endif

  unsigned int subsample = subsampleArg.getValue();

  int degree[DIMENSION];
  for(int i=0; i < DIMENSION; i++){
    degree[i] = degreeArg.getValue();
  }

  //Read Input Image
  ImagePointer image = ImageIO<Image>::readImage(inputArg.getValue());


  ImageRegion region = image->GetLargestPossibleRegion();
  ImageIndex index = region.GetIndex();
  ImageSize size = region.GetSize();
  if(ixArg.getValue() > index[0]){
    index[0] = ixArg.getValue();
  }
  if(iyArg.getValue() > index[1]){
    index[1] = iyArg.getValue();
  }
#if DIMENSION == 3
  if(izArg.getValue() > index[2]){
    index[2] = izArg.getValue();
  }
#endif

  if(sxArg.getValue() > 0){
    size[0] = sxArg.getValue();
  }
  if(syArg.getValue() > 0){
    size[1] = syArg.getValue();
  }
#if DIMENSION == 3
  if(szArg.getValue() > 0){
    size[2] = szArg.getValue();
  }
#endif
  region.SetIndex(index);
  region.SetSize(size);



  double *rangeIndex =  new double[DIMENSION];
  double *rangeSize =  new double[DIMENSION];
  
  int n = 1;
  int m = 1;
  for(int i=0; i < DIMENSION; i++){
    n*= ncps[i];
    m*= size[i];
    rangeIndex[i] = index[i];
    rangeSize[i] = size[i];
    std::cout << "dim: " << i << std::endl;
    std::cout << "size: " << size[i] << std::endl;
    std::cout << "ncps: " << ncps[i] << std::endl;
  }

  m /= pow(subsample, DIMENSION);
  std::cout << "Setting up least squares system" << std::endl;
  //Least squares system
  DenseMatrix<Precision> A(m, n);
  Linalg<Precision>::Zero(A);
  DenseMatrix<Precision> b(m, 1);
  Linalg<Precision>::Zero(b);


  //Setup bspline over image
  BSpline bspline = BSpline::createIdentity( rangeIndex, rangeSize, ncps, degree);
 
  //Get basis functions and controlpoints
  ControlMesh &mesh = bspline.GetControlMesh();
  Knots &uk = bspline.GetUKnots();
  Precision ua = uk.GetKnotA();
  Precision ub = uk.GetKnotB();
  Precision ur = ub - ua;

  Knots &vk = bspline.GetVKnots();
  Precision va = vk.GetKnotA();
  Precision vb = vk.GetKnotB();
  Precision vr = vb - va;
#if DIMENSION == 3
  Knots &wk = bspline.GetWKnots();
  Precision wa = wk.GetKnotA();
  Precision wb = wk.GetKnotB();
  Precision wr = wb - wa;
#endif

  Precision *bfu = uk.BasisFunctions(0);
  Precision *bfv = vk.BasisFunctions(0);
#if DIMENSION == 3
  Precision *bfw = wk.BasisFunctions(0);
#endif

  itk::ImageRegionIteratorWithIndex<Image> it(image, region);
  for(; !it.IsAtEnd(); ++it){
    ImageIndex ind = it.GetIndex();
    bool skip = false;
    for(int i=0; i<DIMENSION;i++){
      ind[i] -= index[i];
      if(ind[i] % subsample != 0){
        skip = true;
        break;
      }
    }
    if(skip){
      continue;
    }
    
    //Compute row index to set value
    int row = ind[0]/subsample + ind[1]/subsample * size[0]/subsample;
#if DIMENSION == 3
    row += ind[2]/subsample * size[0]/subsample * size[1]/subsample;
#endif


    b(row, 0) = it.Get();
    
    //BSpline basis functions
    Precision u = ((Precision) ind[0] * ur) / size[0] + ua; 
    int su = uk.FindSpan(u);
    uk.BasisFunctions(su, u, bfu);
    Precision v = ((Precision) ind[1] * vr) / size[1] + va;
    int sv = vk.FindSpan(v);
    vk.BasisFunctions(sv, v, bfv);
#if DIMENSION == 3 
    Precision w = ((Precision) ind[2] * wr) / size[2] + wa;
    int sw = wk.FindSpan(w);
    wk.BasisFunctions(sw, w, bfw);
#endif

    //std::cout << bfu[2] << ", " << bfv[0] <<", " << bfw[1] << std::endl;

    //Compute column indicies to set values
    for(int i=0; i<= degree[0]; i++){
      for(int j=0; j<=degree[1]; j++){
        int colj = (sv-degree[1]+j)*ncps[0] + su-degree[0]+i;
#if DIMENSION == 3
        for(int k=0; k<=degree[2]; k++){
          int colk = colj +(sw-degree[2]+k)*ncps[0]*ncps[1];
          A(row, colk) = bfu[i] * bfv[j] * bfw[k];
          //std::cout << row <<"," << colk <<": " << A(row, colk) << std::endl;
        }
#else
        A(row, colj) = bfu[i]*bfv[j];
#endif
      }
    }

  }

  //Compute pseudoinverse
  std::cout << "Computeing A^T A" << std::endl;
  DenseMatrix<Precision> ATA = Linalg<Precision>::Multiply(A, A, true, false);
  DenseMatrix<Precision> ATb = Linalg<Precision>::Multiply(A, b, true, false);
  std::cout << " Solving A^T A x = A^T b with lapck dposv" << std::endl;
  DenseMatrix<Precision> x = Linalg<Precision>::SolveSPD(ATA, ATb);


  //std::cout << "Solving least squares with lapack dgels routine" << std::endl;
  //Solve least squares
  //DenseMatrix<Precision> x = Linalg<Precision>::LeastSquares(A, b);
  

  std::cout << "Sampling image from least squares bspline" << std::endl;
  //Set control points
  for(int i=0; i<ncps[0]; i++){
    for(int j=0; j<ncps[1]; j++){
      int rowj = i+j*ncps[0];
#if DIMENSION == 3
      for(int k=0;k<ncps[2]; k++){
        int rowk = rowj + k*ncps[0]*ncps[1];
        mesh.Set(i, j, k, x(rowk, 0));
        //std::cout << rowk << ": " << x(rowk, 0) << std::endl;
      }
#else
      mesh.Set(i, j, x(rowj, 0));
#endif 
    }
  }


  //Write ouput image
  //ImagePointer output = ImageIO<Image>::createImage(image);
  ImagePointer output = ImageIO<Image>::readImage(inputArg.getValue());
  itk::ImageRegionIteratorWithIndex<Image> oit(output, region);

  TControlPoint val;
  for(; !oit.IsAtEnd(); ++oit){
    ImageIndex ind = oit.GetIndex();
    Precision u = ((Precision) (ind[0] - index[0]) * ur) / size[0] + ua; 
    Precision v = ((Precision) (ind[1] - index[1]) * vr) / size[1] + va;
#if DIMENSION == 3 
    Precision w = ((Precision) (ind[2] - index[2]) * wr) / size[2] + wa;
    bspline.PointAt(u, v, w, val);
#else
    bspline.PointAt(u, v, val);
#endif
    //std::cout << val << std::endl;
    oit.Set(val.x);
  }

  ImageIO<Image>::saveImage(output, outputArg.getValue());


  //write bspline
  std::ofstream file;
  file.open("bspline");
  file << bspline << std::endl;
  file.close(); 

  return 0;

}

